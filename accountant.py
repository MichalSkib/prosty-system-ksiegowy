import sys

ALLOWED_COMMANDS = ('saldo', 'zakup', 'sprzedaz', 'stop')

saldo = 1000.0 # POCZATKOWE SALDO
store = {
    'chleb': {'count': 2, 'price': 10.0},
    'mleko': {'count': 12, 'price': 4.0}
} # MAGAZYN

mode = sys.argv[1]

logs = [] # historia operacji

while True:
    command = input('Wpisz komendę: ')

    if command not in ALLOWED_COMMANDS:
        print('Niedozwolona komenda!')
        continue
    if command == 'stop':
        print('Koniec programu!')
        break

    if command == 'saldo':
        amount = float(input('Kwota salda: '))
        if (amount < 0) and (saldo + amount < 0):
            print('Nie masz tyle na koncie!')
            continue
        saldo += amount
        log = f"Zmiana Salda o: {amount}"
        logs.append(log)
    elif command == 'zakup':
        product_name = input('Nazwa produktu: ')
        product_count = int(input('Ilosc sztuk: '))
        product_price = float(input('Cena za sztuke: '))
        product_total_price = product_price * product_count
        if product_total_price > saldo:
            print(f'Cena za towary ({product_total_price}) przekracza wartosc'
                  f'salda ({saldo})')
            continue
        else:
            saldo -= product_total_price
            if not store.get(product_name):
                store[product_name] = {'count': product_count, 'price': product_price}
            else:
                store_product_count = store[product_name]['count']
                store[product_name] = {
                    'count': store_product_count + product_count,
                    'price': product_price}
        log = f'Dokonano zakupu produktu: {product_name} ' \
              f'w ilosci {product_count} sztuk, ' \
              f'o cenie jednostkowej {product_price}.'
        logs.append(log)
    elif command == 'sprzedaz':
        product_name = input('Nazwa produktu: ')
        product_count = int(input('Ilosc sztuk: '))
        product_price = float(input('Cena za sztuke: '))
        if not store.get(product_name):
            print('W magazynie nie ma takiego produktu!')
            continue
        if store.get(product_name)['count'] < product_count:
            print('Brak wystarczajacej ilosci towaru!')
            continue
        store[product_name] = {
            'count': store.get(product_name)['count'] - product_count,
            'price': product_price}
        saldo += product_count * product_price
        if not store.get(product_name)['count']:
            del store[product_name]
        log = f'Dokonano sprzedazy produktu: {product_name} ' \
              f'w ilosci {product_count} sztuk, ' \
              f'o cenie jednostkowej {product_price}.'
        logs.append(log)
if mode == 'konto':
    print(f'SALDO: {saldo}')
elif mode == 'magazyn':
    for product in range(len(sys.argv) - 2):
        print('<' + str(sys.argv[product + 2]) + '> : <' +
              str(store.get(sys.argv[product + 2])['count']) + '>')
elif mode == 'przeglad':
    print(logs)
elif mode == 'saldo':
    amount = float(sys.argv[2])
    if (amount < 0) and (saldo + amount < 0):
        log = f'Nie masz {amount} na koncie!'
        logs.append(log)
    saldo += amount
    log = f"Komentarz: {sys.argv[3]} {amount}"
    logs.append(log)
    print(f'{sys.argv[1]} {sys.argv[2]} {sys.argv[3]}')
    print(logs)
elif mode == 'zakup':
    product_name = sys.argv[2]
    product_count = int(sys.argv[4])
    product_price = float(sys.argv[3])
    product_total_price = product_price * product_count
    if product_total_price > saldo:
        log = f'Cena za towary ({product_total_price}) przekracza wartosc ' \
              f'salda ({saldo})'
        logs.append(log)
    else:
        saldo -= product_total_price
        if not store.get(product_name):
            store[product_name] = {'count': product_count, 'price': product_price}
        else:
            store_product_count = store[product_name]['count']
            store[product_name] = {
                'count': store_product_count + product_count,
                'price': product_price}
    log = f'Dokonano zakupu produktu: {product_name} ' \
          f'w ilosci {product_count} sztuk, ' \
          f'o cenie jednostkowej {product_price}.'
    logs.append(log)
    print(f'{sys.argv[1]} {sys.argv[2]} {sys.argv[3]} {sys.argv[4]}')
    print(logs)
elif mode == 'sprzedaz':
    product_name = sys.argv[2]
    product_price = float(sys.argv[3])
    product_count = int(sys.argv[4])
    if not store.get(product_name):
        log = f'W magazynie nie ma takiego produktu!'
        logs.append(log)
    if store.get(product_name)['count'] < product_count:
        log = f'Brak wystarczajacej ilosci towaru!'
        logs.append(log)
    store[product_name] = {
        'count': store.get(product_name)['count'] - product_count,
        'price': product_price}
    saldo += product_count * product_price
    if not store.get(product_name)['count']:
        del store[product_name]
    log = f'Dokonano sprzedazy produktu: {product_name} ' \
          f'w ilosci {product_count} sztuk, ' \
          f'o cenie jednostkowej {product_price}.'
    logs.append(log)
    print(f'{sys.argv[1]} {sys.argv[2]} {sys.argv[3]} {sys.argv[4]}')
    print(logs)
